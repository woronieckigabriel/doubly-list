#pragma once
#include <string>
#include <iostream>

template <typename T>
class Node {
private:
    T data;
public:
 
    Node(T data);
    Node* next;
    Node* prev;
    Node() = default;
    T getData();
    void setData(T dana);
    T deleteData();
    std::string string();
    ~Node();
};

template<typename T>
class Kontener{
private:
    T* head = NULL;
    T* tail = NULL;
    int size = 0;
public:
    Kontener()=default;
    void addFirst(T* nowy);
    void addLast(T* nowy);
    T* index(int index);
    void del();
    void swap(int a, int b);
    void print();
    void overloadAddLast(T* nowy);
    void overloadAddFirst(T* nowy);
    void save();
    void load();
    int getSize();
    T& operator[](int index);
    ~Kontener();
};

