﻿#include <iostream>
#include <string>
#include "zrodlo.h"
#include "zrodlo.cpp"

int main()
{
    int i,a = 0;
    int b, c = 0;
    int liczba,x;
    char znak;
    bool check=true;
    Kontener<Node<int>> test;
    Kontener<Node<char>> test2;


    std::cout << "Nad ktorym kontenerem chcesz pracowac?" << std::endl;
    std::cout << "1. Kontener intow" << std::endl;
    std::cout << "2. Kontener charow" << std::endl;
    std::cin >> i;
     while (check) {
        switch (i) {
        case 1:
            std::cout << "Testowanie funkcji dla intow:" << std::endl;
            std::cout << "1. Dodawanie nowych danych" << std::endl;
            std::cout << "2. Usuwanie danych" << std::endl;
            std::cout << "3. Zamiana miejscami" << std::endl;
            std::cout << "4. Operator []" << std::endl;
            std::cout << "5. Sprawdz czy obiekt jest w kontenerze" << std::endl;
            std::cout << "6. Zapisz do pliku" << std::endl;
            std::cout << "7. Wczytaj z pliku" << std::endl;
            std::cout << "8. Wypisz" << std::endl;
            std::cout << "0 zeby zamknac" << std::endl;
            std::cin >> a;
            switch (a) {
            case 1:
                std::cout << "Dodaj z poczatku(1) lub na koncu (2)" << std::endl;
                std::cin >> x;
                switch (x) {
                case 1:
                    std::cout << "Podaj liczbe" << std::endl;
                    std::cin >> liczba;
                    test.overloadAddFirst(new Node<int>(liczba));
                    break;
                case 2:
                    std::cout << "Podaj liczbe" << std::endl;
                    std::cin >> liczba;
                    test.overloadAddLast(new Node<int>(liczba));
                    break;

                }
                break;
            case 2:
                test.del();
                break;
            case 3:
                int y, z;
                std::cout << "Podaj pozycje ktore chcesz zamienic miejscami" << std::endl;
                std::cin >> y >> z;
                test.swap(y, z);
                break;
            case 4:
                int ind;
                std::cout << "Podaj indeks znaku" << std::endl;
                std::cin >> ind;
                std::cout << "Liczba o indeksie " << ind << " to " << test[ind].getData() << std::endl;;
                break;
            case 6:
                test.save();
                break;
            case 7:
               // test.load();
                break;
            case 8:
                test.print();
                break;
            case 0:
                check = false;
                break;
            }
            break;
        case 2:
            std::cout << "Testowanie funkcji dla charow:" << std::endl;
            std::cout << "1. Dodawanie nowych danych" << std::endl;
            std::cout << "2. Usuwanie danych" << std::endl;
            std::cout << "3. Zamiana miejscami" << std::endl;
            std::cout << "4. Operator []" << std::endl;
            std::cout << "5. Sprawdz czy obiekt jest w kontenerze" << std::endl;
            std::cout << "6. Zapisz do pliku" << std::endl;
            std::cout << "7. Wczytaj z pliku" << std::endl;
            std::cout << "8. Wypisz" << std::endl;
            std::cout << "0 zeby zamknac" << std::endl;
            std::cin >> b;
            switch (b) {
            case 1:
                std::cout << "Dodaj z poczatku(1) lub na koncu (2)" << std::endl;
                std::cin >> x;
                switch (x) {
                case 1:
                    std::cout << "Podaj znak" << std::endl;
                    std::cin >> znak;
                    test2.overloadAddFirst(new Node<char>(znak));
                    break;
                case 2:
                    std::cout << "Podaj znak" << std::endl;
                    std::cin >> znak;
                    test2.overloadAddLast(new Node<char>(znak));
                    break;                   
                }
                break;
            case 2:
                test2.del();
                break;
            case 3:
                int y, z;
                std::cout << "Podaj pozycje ktore chcesz zamienic miejscami" << std::endl;
                std::cin >> y >> z;
                test2.swap(y, z);
                break;
            case 4:
                int ind;
                std::cout << "Podaj indeks znaku" << std::endl;
                std::cin >> ind;
                std::cout << "Znak o indeksie " << ind << " to " << test2[ind].getData() << std::endl;;
                break;
            case 6:
               test2.save();
                break;

            case 8:
                test2.print();
                break;
            case 0:
                check = false;
                 break;
            }
            break;
        }
    }

}

