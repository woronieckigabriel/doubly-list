#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "zrodlo.cpp"

template <typename T>
T Node<T>::getData()
{
	return this->data;
}

template<typename T>
void Node<T>::setData(T dana)
{
	this->data = dana;
}

template<typename T>
inline T Node<T>::deleteData()
{
	return data = NULL;
}

template<typename T>
inline std::string Node<T>::string()
{
	std::stringstream tekst;
	tekst << this->getData();
	return tekst.str();
}


template <typename T>
Node<T>::Node(T data)
{
	this->data = data;
}

template <typename T>
Node<T>::~Node() 
{
		if (prev != nullptr)
			this->prev->next = next;

		if (next != nullptr)
			this->next->prev = prev;

}

template<typename T>
int Kontener<T>::getSize()
{
	return this->size;
}

template<typename T>
T& Kontener<T>::operator[](int index)
{
	return* this->index(index);
}

template<typename T>
Kontener<T>::~Kontener()
{
	while (this->head) {
		T* next = this->head->next;
		delete this->head;
		this->size = this->size - 1;
		this->head = next;
	}
}

template<typename T>
void Kontener<T>::addFirst(T* nowy)
{
	nowy->next = nullptr;
	nowy->prev = nullptr;
	if (this->head == NULL) {
		this->head = nowy;
		this->tail = this->head;
	}
	else {
		this->head->prev = nowy;
		nowy->next = this->head;
		this->head = nowy;	
	}
	this->size += 1;
}

template<typename T>
void Kontener<T>::addLast(T* nowy)
{
	nowy->next = nullptr;
	nowy->prev = nullptr;
	if (this->tail == NULL) {
		this->tail = nowy;
		this->head = this->tail;
	}
	else {
		this->tail->next = nowy;
		nowy->prev = this->tail;
		this->tail = nowy;
	}
	this->size += 1;
}

template<typename T>
T* Kontener<T>::index(int index)
{
	T* tmp = this->head;
	if (index == 1) {
		return tmp;
	}
	else if(index>this->size){
		std::cout << "Nie dziala" << std::endl;
	}
	else {
		for (int i = 2; i <= index; i++) {
			tmp = tmp->next;
		}
		return tmp;
	}
	
}

template<typename T>
void Kontener<T>::del()
{
	if (this->head == NULL) {
		std::cout << "Nie mozna usunac- lista jest pusta" << std::endl;
	}
	else {
		int count = 0;
		std::cout << "Ktory element chcesz usunac?" << std::endl;
		std::cin >> count;
		if (count > this->size || count <= 0) {
			std::cout << "Nie ma takiego elementu" << std::endl;
		}
		else {
			if (this->size == 2) {
				if (count == 1) {
					this->head = this->tail;
					this->tail->prev = nullptr;
				}
				else if (count == 2) {
					this->tail = this->head;
					this->head->next = nullptr;
				}
				else {
					std::cout << "Nie ma takiego elementu" << std::endl;
				}
			}
			else if (this->size == 1) {
				this->head = nullptr;
				this->tail = nullptr;
			}
			else if (count == 1) {
				T* tmp = this->head;
				tmp->deleteData();
				this->head = tmp->next;
				tmp->prev = nullptr;
				tmp->next = nullptr;
				this->head->prev = nullptr;
			}
			else if (count == this->size) {
				T* tmp = this->tail;
				tmp->deleteData();
				this->tail = tmp->prev;
				tmp->prev = nullptr;
				tmp->next = nullptr;
				this->tail->next = nullptr;
			}
			
			else {
				T* tmp = this->head;
				for (int i=2; i <= count; i++) {
						tmp = tmp->next;
					if (i == count) {
						tmp->deleteData();
						tmp->prev->next = tmp->next; 
						tmp->next->prev = tmp->prev;

					}

				}
			}
			this->size--;
		}
	}

}

template<typename T>
void Kontener<T>::swap(int a, int b)
{
	T* T_a = this->index(a);
	T* T_b = this->index(b);

	T tmp= *T_a;
	T_a->setData(T_b->getData());
	T_b->setData(tmp.getData());

}

template<typename T>
void Kontener<T>::print()
{
	std::cout << std::endl;
	T* tmp = this->head;
	while (tmp) {
		std::cout << tmp->string() << std::endl;
		tmp = tmp->next;
	}
	std::cout << "Size:" << this->size << std::endl;;
	std::cout << std::endl;
}

template<typename T>
void Kontener<T>::overloadAddLast(T* nowy)
{
	this->addLast(nowy);
}

template<typename T>
void Kontener<T>::overloadAddFirst(T* nowy)
{
	this->addFirst(nowy);
}

template<typename T>
void Kontener<T>::save()
{
	std::ofstream zapis("./doublylist.txt");
	if (zapis.good() == true) {
		T* tmp = this->head;
		zapis << this->getSize() << std::endl;;
		for (int i = 0; i < this->size; i++) {
			zapis << tmp->getData() << std::endl;;
			tmp = tmp->next;
		}
		zapis.close();
	}
	else std::cout << "Nie udalo sie otworzyc pliku!" << std::endl;

}

template<typename T>
void Kontener<T>::load()
{
	std::fstream wczytaj("./doublylist.txt");
	if (wczytaj.good() == true) {
		int size;
		wczytaj >> size;
		for (int i = 0; i < size; i++) {
			Node<T>* node = new Node<T>();
			wczytaj >> *node;
			this->overloadAddLast(node);
		}
		wczytaj.close();
	}
	else std::cout << "Nie udalo sie otworzyc pliku!" << std::endl;
}

